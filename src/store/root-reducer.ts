import { Action, Reducer } from 'redux';

export enum ActionType {
  FetchApi = 'FETCH_API',
}

export interface InitialState {
  data: any;
}

export const initialState: InitialState = {
  data: '',
};

export interface DispatchAction extends Action<ActionType> {
  payload: Partial<InitialState>;
}

export const rootReducer: Reducer<InitialState, DispatchAction> = (
  state = initialState,
  action
) => {
  console.log(state);
  if (action.type === ActionType.FetchApi) {
    return { ...state, data: action.payload.data || '' };
  } else return state;
};
