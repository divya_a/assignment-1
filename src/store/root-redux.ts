import { Dispatch } from 'redux';
import { DispatchAction, ActionType } from './root-reducer';

export class RootDispatcher {
  private readonly dispatch: Dispatch<DispatchAction>;

  constructor(dispatch: Dispatch<DispatchAction>) {
    this.dispatch = dispatch;
  }
  fetchAPI = (data: any) =>
    this.dispatch({ type: ActionType.FetchApi, payload: { data } });
}
