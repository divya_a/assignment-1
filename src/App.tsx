import React from 'react';
import { Provider } from 'react-redux';
import FdaPanel from './components/FdaPanel/FdaPanel';
import { store } from './store';
const App: React.FC = () => {
  return (
    <Provider store={store}>
      <div className="App">
        <FdaPanel />
      </div>
    </Provider>
  );
};

export default App;
