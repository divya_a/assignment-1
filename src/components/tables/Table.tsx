import { Table } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import { InitialState } from '../../store/root-reducer';

interface StateProps {
  data: any;
}

const TableView: React.FC = () => {
  const { data } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        data: state.data,
      };
    }
  );

  const columns = [
    {
      title: 'City',
      dataIndex: 'term',
      key: 'term',
      sorter: {
        compare: (a: { term: number }, b: { term: number }) => a.term - b.term,
        multiple: 3,
        sortDirections: ['descend', 'ascend'],
      },
    },
    {
      title: 'Count',
      dataIndex: 'count',
      key: 'count',
      sorter: {
        compare: (a: { term: number }, b: { term: number }) => a.term - b.term,
        multiple: 3,
        sortDirections: ['descend', 'ascend'],
      },
    },
  ];

  return (
    <div style={{ height: 350, width: 250 }}>
      <Table
        dataSource={
          data &&
          data.map((value: any, key: number) =>
            Object.assign({ ...value, key })
          )
        }
        columns={columns}
      />
    </div>
  );
};

export default TableView;
