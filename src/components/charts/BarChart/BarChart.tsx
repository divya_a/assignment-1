import React from 'react';
import { ResponsiveBar } from '@nivo/bar';
import { useSelector } from 'react-redux';
import { InitialState } from '../../../store/root-reducer';
interface StateProps {
  data: any;
}
interface props {}
const BarChart: React.FC<props> = () => {
  const { data } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        data: state.data,
      };
    }
  );

  return (
    <div style={{ height: 550, width: 550 }}>
      {data?.length && (
        <ResponsiveBar
          data={data}
          keys={['term', 'count']}
          indexBy="term"
          margin={{ top: 50, right: 130, bottom: 180, left: 30 }}
          padding={0.3}
          colors={{ scheme: 'category10' }}
          defs={[
            {
              id: 'dots',
              type: 'patternDots',
              background: 'inherit',
              color: '#e8a838',
              size: 4,
              padding: 1,
              stagger: true,
            },
            {
              id: 'lines',
              type: 'patternLines',
              background: 'inherit',
              color: '#97e3d5',
              rotation: -45,
              lineWidth: 6,
              spacing: 10,
            },
          ]}
          borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
          axisTop={null}
          axisRight={null}
          axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'city',
            legendPosition: 'middle',
            legendOffset: 32,
          }}
          axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: 'count',
            legendPosition: 'middle',
            legendOffset: -40,
          }}
          labelSkipWidth={12}
          labelSkipHeight={12}
          labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
          legends={[
            {
              dataFrom: 'keys',
              anchor: 'top',
              direction: 'column',
              justify: false,
              translateX: 120,
              translateY: -56,
              itemsSpacing: 2,
              itemWidth: 95,
              itemHeight: 20,
              itemDirection: 'left-to-right',
              itemOpacity: 0.85,
              symbolSize: 15,
              effects: [
                {
                  on: 'hover',
                  style: {
                    itemOpacity: 1,
                  },
                },
              ],
            },
          ]}
          animate={true}
          motionStiffness={90}
          motionDamping={15}
        />
      )}
    </div>
  );
};
export default BarChart;
