import React from 'react';
import { Chart, Axis, Tooltip, Geom, Legend } from 'bizcharts';
import { useSelector } from 'react-redux';
import { InitialState } from '../../../store/root-reducer';
interface StateProps {
  data: any;
}
const cols = {
  count: { min: 200, max: 700 },
  term: {},
};

interface props {}
const LineChart: React.FC<props> = () => {
  const { data } = useSelector<InitialState, StateProps>(
    (state: InitialState) => {
      return {
        data: state.data,
      };
    }
  );
  return (
    <div style={{ height: 375, width: 500 }}>
      {data?.length && (
        <Chart data={data} scale={cols} padding={[15, 40, 60, 30]} autoFit>
          <Axis name="term" />
          <Axis name="count" />
          <Legend />
          <Tooltip crosshairs={{ type: 'y' }} />
          <Geom type="line" position="term*count" size={5} />
          <Geom
            type="point"
            position="term*count"
            size={4}
            shape={'circle'}
            style={{ stroke: '#fff', lineWidth: 1 }}
          />
        </Chart>
      )}
    </div>
  );
};
export default LineChart;
