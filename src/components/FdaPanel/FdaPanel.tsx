import React, { useEffect } from 'react';
import { Row, Card } from 'antd';
import { useDispatch } from 'react-redux';
import { RootDispatcher } from '../../store/root-redux';
import axios from 'axios';
import TableView from '../tables/Table';
import BarChart from '../charts/BarChart/BarChart';
import LineChart from '../charts/LineChart/LineChart';

interface Props {}

const FdaPanel: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const rootDispatcher = new RootDispatcher(dispatch);
  const fetchData = async () => {
    await axios
      .get('https://api.fda.gov/food/enforcement.json?count=city.exact&limit=7')
      .then((response) => {
        rootDispatcher.fetchAPI(response.data.results);
      });
  };
  useEffect(() => {
    fetchData();
  });

  return (
    <Row gutter={14}>
      <Card title="Here is your Data!" bordered={false} style={{ width: 300 }}>
        <TableView />
      </Card>

      <Card title="Bar Chart" bordered={false} style={{ width: 450 }}>
        <BarChart />
      </Card>

      <Card title="Line Chart" bordered={false} style={{ width: 450 }}>
        <br />
        <br />
        <LineChart />
      </Card>
    </Row>
  );
};

export default FdaPanel;
